//
//  NextViewController.swift
//  ExpenseTracker
//
//  Created by Carissa on 10/5/17.
//  Copyright © 2017 Carissa. All rights reserved.
//

import UIKit
import CoreData

class NextViewController: UIViewController {
    
    var expense: Expenses?
    
    @IBOutlet var companyName: UITextField!
    @IBOutlet var dateLabel: UITextField!
    @IBOutlet var costLabel: UITextField!
    @IBOutlet var payType: UITextField!
    @IBOutlet var notesView: UITextView!
    
    var managedObjectContext: NSManagedObjectContext?
    
    @IBAction func saveBtn(_ sender: UIBarButtonItem) {
        guard let managedObjectContext = managedObjectContext else { return }
        
        if expense == nil {
            // Create Expense
            let newExpense = Expenses(context: managedObjectContext)
            
            // Configure Expense
            newExpense.createdAt = Date().timeIntervalSince1970
            
            // Set Expense
            expense = newExpense
        }
        
            if let expense = expense {
                expense.companyName = companyName.text
                expense.date = dateLabel.text
                expense.price = costLabel.text
                expense.payType = payType.text
                expense.notes = notesView.text
            }
        
        // Pop View Controller
        _ = navigationController?.popViewController(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Add Expenses"
        
        if let expense = expense {
            companyName.text = expense.companyName
            dateLabel.text = expense.date
            costLabel.text = expense.price
            payType.text = expense.payType
            notesView.text = expense.notes
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
