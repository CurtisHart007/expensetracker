//
//  CustomTableViewCell.swift
//  ExpenseTracker
//
//  Created by Carissa on 10/5/17.
//  Copyright © 2017 Carissa. All rights reserved.
//

import UIKit

class CustomTableViewCell: UITableViewCell {
    
    static let reuseIdentifier = "CustomCell"

    @IBOutlet var companyLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var priceLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
